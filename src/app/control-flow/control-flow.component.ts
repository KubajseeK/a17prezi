import { Component } from '@angular/core';
import { NgForOf, NgIf, NgSwitch, NgSwitchCase, NgSwitchDefault } from "@angular/common";

@Component({
  selector: 'app-control-flow',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    NgSwitch,
    NgSwitchCase,
    NgSwitchDefault
  ],
  templateUrl: './control-flow.component.html',
})
export class ControlFlowComponent {
  protected a: number = 10;
  protected b: number = 5;

  protected elClasicoWinner: string = 'Draw'

  protected streamingServices: string[] = ['Netflix', 'HBO Max', 'Prime Video', 'Disney+']

  function(): void {
    this.streamingServices.length = 0
  }

}
