import { Component } from '@angular/core';
import { MyErrorComponent } from "./my-error/my-error.component";
import { MyLoadingComponent } from "./my-loading/my-loading.component";
import { MyPlaceholderComponent } from "./my-placeholder/my-placeholder.component";
import { MyContentComponent } from "./my-content/my-content.component";

@Component({
  selector: 'app-defer',
  standalone: true,
  imports: [
    MyErrorComponent,
    MyLoadingComponent,
    MyPlaceholderComponent,
    MyContentComponent
  ],
  templateUrl: './defer.component.html'
})
export class DeferComponent {

  protected MIN_WAIT_TIME: number = 1000;

}
