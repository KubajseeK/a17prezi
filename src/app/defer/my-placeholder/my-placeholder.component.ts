import { Component } from '@angular/core';

@Component({
  selector: 'app-my-placeholder',
  standalone: true,
  imports: [],
  templateUrl: './my-placeholder.component.html',
})
export class MyPlaceholderComponent {

}
