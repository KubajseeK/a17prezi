import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-typed-forms',
  standalone: true,
  imports: [],
  templateUrl: './typed-forms.component.html',
})
export class TypedFormsComponent {
  //First Example
  private partyForm = new FormGroup({
    address: new FormGroup({
      number: new FormControl(27),
      street: new FormControl('Štúrova')
    }),
    formal: new FormControl(false),
    foodOptions: new FormArray([])
  })
  private place = this.partyForm.get('address.number')!.value;
  private firstLetter = this.place?.substring(1); //Angular <= 13 by toto neodchytil
  function() {
  }
}
