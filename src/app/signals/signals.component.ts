import { Component, computed, effect, OnInit, signal } from '@angular/core';

@Component({
  selector: 'app-signals',
  standalone: true,
  imports: [],
  templateUrl: './signals.component.html',
})
export class SignalsComponent  {

  readonly exampleSignal = signal("Example")
  readonly count = signal(1)
  doubleCount = computed(() => {
    return this.count() * 2
  });

  constructor() {
    effect(() => {
      console.log(`The count is ${this.count()} and the Double of that is ${this.doubleCount()}`)
    });
  }

  function() {
    this.exampleSignal.update(v => `Predtým to bol ${v}`)
  }

  incrementCount() {
    this.count.update(value => value + 2);
  }
}
