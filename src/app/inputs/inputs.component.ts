import { Component, Input } from '@angular/core';
import { RouterLink } from "@angular/router";

@Component({
  selector: 'app-inputs',
  standalone: true,
  imports: [
    RouterLink
  ],
  templateUrl: './inputs.component.html',
})
export class InputsComponent {
  @Input({required: true}) name: string | undefined

  @Input('id') inputId: string = '0';
}
