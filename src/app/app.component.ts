import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { FormControl } from '@angular/forms';
import { InputsComponent } from "./inputs/inputs.component";
import { ControlFlowComponent } from "./control-flow/control-flow.component";
import { DeferComponent } from "./defer/defer.component";
import { SignalsComponent } from "./signals/signals.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, InputsComponent, RouterLink, ControlFlowComponent, DeferComponent, SignalsComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'A17Prezi';

  nameControlExample() {
    const nameControl = new FormControl("John");
    console.log(typeof (nameControl.value));
  }
}
