import { Routes } from '@angular/router';
import { InputsComponent } from "./inputs/inputs.component";

export const routes: Routes = [
  {
    path: 'inputs/:id',
    component: InputsComponent
  }
];
